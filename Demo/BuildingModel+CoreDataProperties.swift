//
//  BuildingModel+CoreDataProperties.swift
//  Demo
//
//  Created by Rajesh Kollipara on 08/02/19.
//  Copyright © 2019 Rajesh Kollipara. All rights reserved.
//
//

import Foundation
import CoreData


extension BuildingModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BuildingModel> {
        return NSFetchRequest<BuildingModel>(entityName: "BuildingModel")
    }

    @NSManaged public var latitude: Double
    @NSManaged public var floor: Int32
    @NSManaged public var date: NSDate?
    @NSManaged public var longitude: Double

}
