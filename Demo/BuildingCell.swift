//
//  BuildingCell.swift
//  Demo
//
//  Created by Rajesh Kollipara on 08/02/19.
//  Copyright © 2019 Rajesh Kollipara. All rights reserved.
//

import UIKit

class BuildingCell: UITableViewCell {
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelFloor: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
