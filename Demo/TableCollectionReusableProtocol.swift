

import Foundation


import Foundation
import UIKit
protocol Reusable {
    
}
extension Reusable {
    static var reusableIdentifier: String {
        return String(describing: self)
    }
}

extension UIView: Reusable {}

extension UITableView {
    
    func registerCell<T: UITableViewCell>(_ : T.Type)  {
        self.register(T.self, forCellReuseIdentifier: T.reusableIdentifier)
    }
    
    func registerNib<T: UITableViewCell>(_ : T.Type) {
        self.register(UINib(nibName: T.reusableIdentifier, bundle: nil), forCellReuseIdentifier: T.reusableIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T  {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.reusableIdentifier, for: indexPath) as? T else {
            fatalError("Could not deque cell")
        }
        return cell
    }
}
