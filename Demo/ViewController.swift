//
//  ViewController.swift
//  Demo
//
//  Created by Rajesh Kollipara on 08/02/19.
//  Copyright © 2019 Rajesh Kollipara. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
let appDelegate = UIApplication.shared.delegate as! AppDelegate

class ViewController: UIViewController {
    @IBOutlet weak var buildingInfoTable: UITableView!
    @IBOutlet weak var fieldLatitude: UITextField!
    @IBOutlet weak var fieldLongitude: UITextField!
    let formatter = DateFormatter()
    @IBOutlet weak var fieldFloor: UITextField!
    let context = appDelegate.persistentContainer.viewContext
    
    var tableData =  [BuildingModel]() {
        
        didSet {
            
            buildingInfoTable.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        buildingInfoTable.tableFooterView = UIView()
        retrieveInfoFromCoreData()
        
    }
    
    @IBAction func actionStoreInfo(_ sender: UIButton) {
        view.endEditing(true)
        let buildingModel = NSEntityDescription.insertNewObject(forEntityName: "BuildingModel", into: context) as! BuildingModel
        
        guard let lat = fieldLatitude.text , let long =  fieldLongitude.text, let floor = fieldFloor.text else {
            
            return
        }
        
        let latitude = (lat as NSString).doubleValue
        let longitude = (long as NSString).doubleValue
        let floorNumber = (floor as NSString).intValue
        
        guard CLLocationCoordinate2DIsValid(CLLocationCoordinate2D(latitude: latitude, longitude: longitude)) else {
            
            self.showAlertviewController(titleName: "", messageName: "Please enter valid values", cancelButtonTitle: "OK", otherButtonTitles: [], selectionHandler: {_ in })
            return
        }
        
        buildingModel.date = Date() as NSDate
        buildingModel.floor = floorNumber
        buildingModel.latitude = latitude
        buildingModel.longitude = longitude
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Saving error: \(error.localizedDescription)")
        }
        retrieveInfoFromCoreData()
    }
    
    private func resetValues() {
        self.fieldLatitude.text = ""
        self.fieldLongitude.text = ""
        self.fieldFloor.text = ""
    }
    
    func retrieveInfoFromCoreData() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BuildingModel")
        let sort = NSSortDescriptor(key: #keyPath(BuildingModel.date), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        guard let buildingData = try? context.fetch(request) as? [BuildingModel], let validBuildingArray =  buildingData else { return }
        self.tableData = validBuildingArray
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let buildingCell =  tableView.dequeueReusableCell(forIndexPath: indexPath) as BuildingCell
        let model = self.tableData[indexPath.row]
        buildingCell.labelFloor.text = "\(model.floor)"
        buildingCell.labelTime.text = (model.date as Date?)?.string(with: "dd/MM/yyyy HH:mm:ss")
        buildingCell.labelLocation.text = "( \(model.latitude) , \(model.longitude) )"
        return buildingCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension Date {
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIViewController {
    
    func showAlertviewController(titleName: String, messageName: String, cancelButtonTitle: String, otherButtonTitles: [String], selectionHandler: @escaping (Int) -> Void ) {
        
        let alertController = UIAlertController(title: NSLocalizedString(titleName, comment: ""), message: NSLocalizedString(messageName, comment: ""), preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: NSLocalizedString(cancelButtonTitle, comment: ""), style: .cancel, handler: { alert in
            
            selectionHandler(alertController.actions.index(of: alert)!)
        })
        otherButtonTitles.forEach {
            
            let alertAction = UIAlertAction(title: NSLocalizedString($0, comment: ""), style: .default, handler: { alert in
                selectionHandler(alertController.actions.index(of: alert)!)
            })
            alertController.addAction(alertAction)
            
        }
        
        alertController.addAction(alertActionOk)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
